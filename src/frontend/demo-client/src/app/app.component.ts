import { ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {IRapidCustomer, IRapidOrder } from './rapidcustomer';
import { RapidCustomerService } from './rapidcustomer.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [RapidCustomerService],
})

export class AppComponent implements OnInit {
  title = 'Rapid customer orders demo';
  searchTerm = '';
  selectedCustomerName = '';
  customers: IRapidCustomer[];
  customerOrders: IRapidOrder[];
  constructor(private _customerService: RapidCustomerService, private _cdRef: ChangeDetectorRef) {
  }
  ngOnInit(): void {
    console.log('AppComponent::ngOnInit');
    this._customerService.getCustomers()
      .subscribe(x => this.customers = x);
  }
  handleSearchChanged(searchInput: string): void {
    // alert(searchInput)
    console.log('handleSearchChanged');
    this.selectedCustomerName = '';
    this.customerOrders = null;
    this._customerService.searchCustomers(searchInput)
      .subscribe(x => this.customers = x);
  }
  private updateCustomerOrders(customer: IRapidCustomer, orders: IRapidOrder []) {
    this.selectedCustomerName = customer.firstName + ' ' + customer.lastName;
    this.customerOrders = orders;
    this._cdRef.detectChanges();
  }
  handleCustomerSelected(customer: IRapidCustomer): void {
    console.log('handleCustomerSelected', customer.id);
    this._customerService.getCustomerOrders(customer.id)
      .subscribe(orders => this.updateCustomerOrders(customer, orders));
  }
}

