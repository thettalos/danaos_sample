import {Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import {IRapidCustomer, IRapidOrder} from './rapidcustomer';

@Injectable()
export class RapidCustomerService {
  private _customersUrl = 'http://localhost:8080/danaos-sample/api/v1/customers';

  constructor(private _http: Http) {
    console.log('CustomerService::ctor');
  }

  getCustomers(): Observable<IRapidCustomer[]> {
    console.log('CustomerService::getCustomers')
    return this._http.get(this._customersUrl)
      .map((response: Response) => <IRapidCustomer[]> response.json().items)
      .do(data => console.log(JSON.stringify(data)));
  }

  searchCustomers(lastName: string): Observable<IRapidCustomer[]> {
    console.log('CustomerService::searchCustomers');
    const searchUrl = this._customersUrl + '/search?lastName=' + lastName;
    return this._http.get(searchUrl)
      .map((response: Response) => <IRapidCustomer[]> response.json().items)
      .do(data => console.log(JSON.stringify(data)));
  }

  getCustomerOrders(customerId: number): Observable<IRapidOrder[]> {
    return this._http.get(this._customersUrl + '/' + customerId + '/orders')
      .map((response: Response) => <IRapidOrder[]> response.json().items)
      .do(data => console.log(JSON.stringify(data)));
  }

}
