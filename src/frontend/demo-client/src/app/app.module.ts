import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { MdCommonModule, MdListModule, MdCardModule, MdInputModule, MdFormFieldModule } from '@angular/material';


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    BrowserAnimationsModule,
    FormsModule,
    MdCommonModule,
    MdListModule,
    MdCardModule,
    MdInputModule,
    MdFormFieldModule,
  ],
  exports: [
    MdCommonModule,
    MdListModule,
    MdCardModule,
    MdInputModule,
    MdFormFieldModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
