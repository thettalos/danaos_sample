import { TestBed, inject } from '@angular/core/testing';

import { RapidCustomerService } from './rapidcustomer.service';

describe('RapidcustomerService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RapidCustomerService]
    });
  });

  it('should be created', inject([RapidCustomerService], (service: RapidCustomerService) => {
    expect(service).toBeTruthy();
  }));
});
