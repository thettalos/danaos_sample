export interface IRapidCustomer {
  id: number;
  firstName: string;
  lastName: string;
}
export interface IRapidProduct {
  id: number;
  name: string;
  unitPrice: number;
}
export interface IRapidOrder {
  id: number;
  itemCount: number;
  product: IRapidProduct;
  customer: IRapidCustomer;
}
