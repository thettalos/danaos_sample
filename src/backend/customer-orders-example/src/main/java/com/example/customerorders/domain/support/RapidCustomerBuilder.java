package com.example.customerorders.domain.support;

import com.example.customerorders.domain.RapidCustomer;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class RapidCustomerBuilder extends EntityBuilder<RapidCustomer> {
    @Override
    void initProduct() {
        this.product = new RapidCustomer();
    }
    public RapidCustomerBuilder firstName(String value) {
        this.product.setFirstName(value);
        return this;
    }
    public RapidCustomerBuilder lastName(String value) {
        this.product.setLastName(value);
        return this;
    }
    public RapidCustomerBuilder dateRegistered(Date value) {
        this.product.setDateRegistered(value);
        return this;
    }

    @Override
    RapidCustomer assembleProduct() {
        return this.product;
    }
}
