package com.example.customerorders.web.api;

import com.example.customerorders.domain.RapidCustomer;
import com.example.customerorders.domain.RapidOrder;
import com.example.customerorders.service.RapidCustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.List;
import java.util.Map;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/v1/customers")
public class RapidCustomerRestController {
    @Autowired
    private RapidCustomerService rapidCustomerService;

    @CrossOrigin(origins = "*")
    @RequestMapping(method = { RequestMethod.GET }, value = "/search")
    public RestCollection<RapidCustomer> getByLastName(@RequestParam Map<String, String> queryMap) {
        String lastName = queryMap.get("lastName");
        Collection<RapidCustomer> customers = this.rapidCustomerService.findCustomers(lastName);
        return new RestCollection<RapidCustomer>(customers);
    }
    @CrossOrigin(origins = "*")
    @RequestMapping(method = { RequestMethod.GET }, value="/{customerId}")
    public RapidCustomerResource getById(@PathVariable long customerId) {
        RapidCustomer customer = this.rapidCustomerService.getCustomer(customerId);
        return new RapidCustomerResource(customer);
    }
    @CrossOrigin(origins = "*")
    @RequestMapping(method = { RequestMethod.GET }, value="/{customerId}/orders")
    public RestCollection<RapidOrder> getOrders(@PathVariable long customerId) {
        List<RapidOrder> customerOrders = this.rapidCustomerService.getCustomerOrders(customerId);
        return new RestCollection<RapidOrder>(customerOrders);
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(method = { RequestMethod.GET })
    public RestCollection<RapidCustomer> getAll() {
        Collection<RapidCustomer> customers = this.rapidCustomerService.allCustomers();
        return new RestCollection<RapidCustomer>(customers);
    }
}
