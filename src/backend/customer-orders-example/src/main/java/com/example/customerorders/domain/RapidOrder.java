package com.example.customerorders.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
public class RapidOrder implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne(optional = false)
    private RapidCustomer customer;

    @ManyToOne(optional = false)
    private RapidProduct product;

    private int itemCount;
    private Date dateOrdered;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public RapidCustomer getCustomer() {
        return customer;
    }

    public void setCustomer(RapidCustomer customer) {
        this.customer = customer;
    }

    public RapidProduct getProduct() {
        return product;
    }

    public void setProduct(RapidProduct product) {
        this.product = product;
    }

    public int getItemCount() {
        return itemCount;
    }

    public void setItemCount(int itemCount) {
        this.itemCount = itemCount;
    }

    public Date getDateOrdered() {
        return dateOrdered;
    }

    public void setDateOrdered(Date dateOrdered) {
        this.dateOrdered = dateOrdered;
    }
}
