package com.example.customerorders.web.api;

import com.example.customerorders.domain.Book;
import com.example.customerorders.domain.BookSearchCriteria;
import com.example.customerorders.service.BookstoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Resources;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.Collection;
import java.util.List;

import static java.util.stream.Collectors.toList;

@RestController
@RequestMapping("/api/v1/books")
public class BookRestController {
    @Autowired
    private BookstoreService bookstoreService;

    @ModelAttribute
    public BookSearchCriteria criteria() {
        return new BookSearchCriteria();
    }

    @RequestMapping(method = { RequestMethod.GET }, value = "/{bookId}")
    public RestResource<Book> readBook(@PathVariable long bookId) {
        Book book = this.bookstoreService.findBook(bookId);
        return new RestResource<Book>(book);
    }

    @RequestMapping(method = { RequestMethod.GET })
    public RestCollection<Book> readBooks() {
        Collection<Book> books = this.bookstoreService.findBooks(new BookSearchCriteria());
        return new RestCollection<Book>(books);
    }

}
