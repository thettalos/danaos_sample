package com.example.customerorders.service;

import com.example.customerorders.domain.RapidCustomer;
import com.example.customerorders.domain.RapidOrder;
import com.example.customerorders.repository.CategoryRepository;
import com.example.customerorders.repository.RapidCustomerRepository;
import com.example.customerorders.repository.RapidOrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service("rapidCustomerService")
@Transactional(readOnly = true)
public class RapidCustomerServiceImpl implements RapidCustomerService {

    @Autowired
    private RapidCustomerRepository customerRepository;

    @Autowired
    private RapidOrderRepository orderRepository;

    @Override
    public RapidCustomer getCustomer(long customerId) {
        return customerRepository.getById(customerId);
    }

    @Override
    public List<RapidCustomer> allCustomers() {
        return customerRepository.getAll();
    }

    @Override
    public List<RapidCustomer> findCustomers(String lastName) {
        return customerRepository.findByLastName(lastName);
    }

    @Override
    public List<RapidOrder> getCustomerOrders(long customerId) {
        RapidCustomer customer = customerRepository.getById(customerId);
        return orderRepository.findCustomerOrders(customer);
    }
}
