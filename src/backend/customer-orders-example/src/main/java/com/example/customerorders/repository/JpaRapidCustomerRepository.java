package com.example.customerorders.repository;

import com.example.customerorders.domain.Account;
import com.example.customerorders.domain.Book;
import com.example.customerorders.domain.RapidCustomer;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

@Repository("rapidCustomerRepository")
public class JpaRapidCustomerRepository implements RapidCustomerRepository {
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<RapidCustomer> getAll() {
        String hql = "select rc from RapidCustomer rc";
        TypedQuery<RapidCustomer> query = this.entityManager.createQuery(hql, RapidCustomer.class);
        return query.getResultList();
    }

    @Override
    public RapidCustomer getById(long customerId) {
        return this.entityManager.find(RapidCustomer.class, customerId);
    }

    @Override
    public List<RapidCustomer> findByLastName(String lastName) {
        String hql = "select rc from RapidCustomer rc where rc.lastName like :lastName";
        TypedQuery<RapidCustomer> query = this.entityManager.createQuery(hql, RapidCustomer.class);
        query.setParameter("lastName", '%' + lastName + '%');
        return query.getResultList();
    }
}
