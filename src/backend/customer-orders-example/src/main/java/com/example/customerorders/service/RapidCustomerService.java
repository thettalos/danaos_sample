package com.example.customerorders.service;

import com.example.customerorders.domain.RapidCustomer;
import com.example.customerorders.domain.RapidOrder;

import java.util.List;

public interface RapidCustomerService {
    RapidCustomer getCustomer(long customerId);
    List<RapidCustomer> allCustomers();
    List<RapidCustomer> findCustomers(String lastName);
    List<RapidOrder> getCustomerOrders(long customerId);
}
