package com.example.customerorders.web.api;

import com.example.customerorders.domain.Book;
import org.springframework.hateoas.ResourceSupport;

public class RestResource<DomainEntityT> {
    private final DomainEntityT item;
    public RestResource(DomainEntityT item){
        this.item = item;
    }

    public DomainEntityT getItem() {
        return item;
    }
}
