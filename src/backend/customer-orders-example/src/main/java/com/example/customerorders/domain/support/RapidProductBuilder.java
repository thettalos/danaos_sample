package com.example.customerorders.domain.support;

import com.example.customerorders.domain.RapidProduct;
import org.springframework.stereotype.Component;

@Component
public class RapidProductBuilder extends EntityBuilder<RapidProduct> {
    @Override
    void initProduct() {
        this.product = new RapidProduct();
    }

    @Override
    RapidProduct assembleProduct() {
        return this.product;
    }
    public RapidProductBuilder name(String value){
        this.product.setName(value);
        return this;
    }
    public RapidProductBuilder unitPrice(double value){
        this.product.setUnitPrice(value);
        return this;
    }
}
