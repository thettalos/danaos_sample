package com.example.customerorders.repository;

import com.example.customerorders.domain.RapidCustomer;
import com.example.customerorders.domain.RapidOrder;

import java.util.List;

public interface RapidOrderRepository {
    List<RapidOrder> findCustomerOrders(RapidCustomer customer);
}
