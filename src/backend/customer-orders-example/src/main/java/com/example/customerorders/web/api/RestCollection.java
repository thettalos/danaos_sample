package com.example.customerorders.web.api;

import java.util.Collection;

public class RestCollection<DomainEntityT> {
    private final Collection<DomainEntityT> items;
    public RestCollection(Collection<DomainEntityT> items) {
        this.items = items;
    }
    public int getSize() {
        return items.size();
    }
    public Collection<DomainEntityT> getItems() {
        return items;
    }
}
