package com.example.customerorders.web.api;

import com.example.customerorders.domain.RapidOrder;

public class RapidOrderResource extends RestResource<RapidOrder> {
    public RapidOrderResource(RapidOrder item) {
        super(item);
    }
}
