package com.example.customerorders.web.controller;

import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.example.customerorders.domain.Book;
import com.example.customerorders.domain.BookSearchCriteria;
import com.example.customerorders.domain.Category;
import com.example.customerorders.service.BookstoreService;

/**
 * Controller to handle book search requests.
 * 
 * 
 * 
 *
 */
@Controller
public class BookSearchController {

    @Autowired
    private BookstoreService bookstoreService;

    @ModelAttribute
    public BookSearchCriteria criteria() {
        return new BookSearchCriteria();
    }

    @ModelAttribute("categories")
    public List<Category> getCategories() {
        List<Category> r = this.bookstoreService.findAllCategories();
        return r;
    }

    /**
     * This method searches our database for books based on the given {@link BookSearchCriteria}.
     * Only books matching the criteria are returned.
     *
     * @param criteria the criteria used for searching
     * @return the found books
     *
     * @see com.example.customerorders.repository.BookRepository#findBooks(BookSearchCriteria)
     */
    @RequestMapping(value = "/book/search", method = { RequestMethod.GET })
    public Collection<Book> list(@ModelAttribute("bookSearchCriteria") BookSearchCriteria criteria) {
        Collection<Book> r = this.bookstoreService.findBooks(criteria);
        return r;
    }
    @RequestMapping(value = "/book/all", method = { RequestMethod.GET })
    public Collection<Book> listAll() {
        Collection<Book> r = this.bookstoreService.findBooks(new BookSearchCriteria());
        return r;
    }

}
