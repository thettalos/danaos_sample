package com.example.customerorders.repository;

import com.example.customerorders.domain.RapidCustomer;
import com.example.customerorders.domain.RapidOrder;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

@Repository("rapidOrderRepository")
public class JpaRapidOrderRepository implements RapidOrderRepository {
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<RapidOrder> findCustomerOrders(RapidCustomer customer) {
        String hql = "select ro from RapidOrder ro where ro.customer.id = :customerId";
        TypedQuery<RapidOrder> query = this.entityManager.createQuery(hql, RapidOrder.class);
        query.setParameter("customerId", customer.getId());
        return query.getResultList();
    }
}
