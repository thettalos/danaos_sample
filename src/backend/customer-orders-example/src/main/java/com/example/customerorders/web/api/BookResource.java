package com.example.customerorders.web.api;

import com.example.customerorders.domain.Book;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class BookResource extends RestResource<Book>{
    public BookResource(Book item) {
        super(item);
    }
}
