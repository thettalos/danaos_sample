package com.example.customerorders.service;

import java.util.List;

import com.example.customerorders.domain.Category;

/**
 * Contract for services that work with an {@link Category}.
 * 
 * 
 * 
 * 
 */
public interface CategoryService {

	Category findById(long id);

	List<Category> findAll();

	void addCategory(Category category);

}
