package com.example.customerorders.domain.support;

import com.example.customerorders.domain.RapidCustomer;
import com.example.customerorders.domain.RapidOrder;
import com.example.customerorders.domain.RapidProduct;
import org.springframework.stereotype.Component;

@Component
public class RapidOrderBuilder extends EntityBuilder<RapidOrder> {
    @Override
    void initProduct() {
        this.product = new RapidOrder();
    }

    @Override
    RapidOrder assembleProduct() {
        return this.product;
    }
    public RapidOrderBuilder customer(RapidCustomer value){
        this.product.setCustomer(value);
        return this;
    }
    public RapidOrderBuilder product(RapidProduct value){
        this.product.setProduct(value);
        return this;
    }
    public RapidOrderBuilder itemCount(int value){
        this.product.setItemCount(value);
        return this;
    }
}
