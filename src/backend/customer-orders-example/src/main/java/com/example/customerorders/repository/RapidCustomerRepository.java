package com.example.customerorders.repository;

import com.example.customerorders.domain.RapidCustomer;
import com.example.customerorders.domain.RapidOrder;

import java.util.List;

public interface RapidCustomerRepository {
    List<RapidCustomer> getAll();
    RapidCustomer getById(long customerId);
    List<RapidCustomer> findByLastName(String lastName);
}
