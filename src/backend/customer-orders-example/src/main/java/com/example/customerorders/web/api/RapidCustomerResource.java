package com.example.customerorders.web.api;

import com.example.customerorders.domain.RapidCustomer;

public class RapidCustomerResource extends RestResource<RapidCustomer> {
    public RapidCustomerResource(RapidCustomer item) {
        super(item);
    }
}
